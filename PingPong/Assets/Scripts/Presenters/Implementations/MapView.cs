﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PresentersImpl
{
    public class MapView : ViewBase
    {
        [SerializeField] private Transform leftBorder;
        [SerializeField] private Transform rightBorder;

        public void UpdateBorders(float left, float right)
        {
            leftBorder.position = new Vector2(left, 0f);
            rightBorder.position = new Vector2(right, 0f);
        }
    }
}
