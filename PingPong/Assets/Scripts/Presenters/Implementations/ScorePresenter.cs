﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ViewActions;
using GameRulesInterfaces;
using PresentersInterfaces;
using GameRulesActions;

namespace PresentersImpl
{
    public class ScorePresenter : PresenterBase
    {
        public ScorePresenter(IView view, IGameRules gameRules) : base(view, gameRules)
        {
        }

        protected override void GameRulesActionCallback(GameRulesAction action, object data)
        {
            if (action == GameRulesAction.UpdateCurrentScore)
            {
                (_view as ScoreView).UpdateCurrentScore((int)data);
            }

            if (action == GameRulesAction.UpdateBestScore)
            {
                (_view as ScoreView).UpdateBestScore((int)data);
            }
        }

        protected override void ViewActionCallback(ViewAction action, object data)
        {
            
        }
    }
}
