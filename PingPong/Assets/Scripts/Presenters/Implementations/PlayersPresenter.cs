﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ViewActions;
using GameRulesInterfaces;
using PresentersInterfaces;
using GameRulesActions;

namespace PresentersImpl
{
    public class PlayersPresenter : PresenterBase
    {
        public PlayersPresenter(IView view, IGameRules gameRules) : base(view, gameRules)
        {
        }

        protected override void GameRulesActionCallback(GameRulesAction action, object data)
        {
            if (action == GameRulesAction.UpdatePlayer)
            {
                if (data is IPlayer player)
                {
                    UpdatePlayer(player);
                }
            }

            if (action == GameRulesAction.UpdateMap)
            {
                if (data is IMap map)
                {
                    UpdateBorders(map);
                }
            }
        }

        protected override void ViewActionCallback(ViewAction action, object data)
        {

        }

        private void UpdatePlayer(IPlayer player)   
        {
            (_view as PlayersView).UpdatePlayer(player.GetId(), player.GetSpeed(), player.GetMoveDirection());
        }

        private void UpdateBorders(IMap map)
        {
            (_view as PlayersView).UpdateBorders(map.GetTop(), map.GetBottom(), map.GetLeft(), map.GetRight());
        }
    }
}
