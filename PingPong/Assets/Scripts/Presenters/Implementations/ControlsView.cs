﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ViewActions;

namespace PresentersImpl
{
    public class ControlsView : ViewBase
    {
        public void Update()
        {
            if (Input.GetKey("left"))
            {
                _action?.Invoke(ViewAction.MovePlayer, -1);
                return;
            }

            if (Input.GetKey("right"))
            {
                _action?.Invoke(ViewAction.MovePlayer, 1);
                return;
            }

            if (Input.GetKeyUp("left") || Input.GetKeyUp("right"))
            {
                _action?.Invoke(ViewAction.MovePlayer, 0);
                return;
            }
        }
    }
}
