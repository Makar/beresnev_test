﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ViewActions;
using GameRulesInterfaces;
using PresentersInterfaces;
using GameRulesActions;

namespace PresentersImpl
{
    public class MapPresenter : PresenterBase
    {
        public MapPresenter(IView view, IGameRules gameRules) : base(view, gameRules)
        {
        }

        protected override void GameRulesActionCallback(GameRulesAction action, object data)
        {
            if (action == GameRulesAction.UpdateMap)
            {
                if (data is IMap map)
                {
                    UpdateBorders(map);
                }
            }
        }

        protected override void ViewActionCallback(ViewAction action, object data)
        {
            
        }

        private void UpdateBorders(IMap map)
        {
            (_view as MapView).UpdateBorders(map.GetLeft(), map.GetRight());
        }
    }
}
