﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PresentersInterfaces;
using System;
using ViewActions;

namespace PresentersImpl
{
    public class ViewBase : MonoBehaviour, IView
    {
        protected Action<ViewAction, object> _action;

        public void Bind(Action<ViewAction, object> actionCallback)
        {
            _action = actionCallback;
        }

        public void Hide()
        {
            gameObject.SetActive(false);
        }

        public virtual void Init()
        {
            
        }

        public void Show()
        {
            gameObject.SetActive(true);
        }
    }
}