﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ViewActions;

namespace PresentersImpl
{
    public class MobileControlsView : ViewBase
    {
        public void Update()
        {
            if (Input.touchCount > 0)
            {
                Touch touch = Input.GetTouch(0);

                if (touch.position.x > Screen.width / 2)
                {
                    _action?.Invoke(ViewAction.MovePlayer, 1);
                }
                else
                {
                    _action?.Invoke(ViewAction.MovePlayer, -1);
                }
            }
            else
            {
                _action?.Invoke(ViewAction.MovePlayer, 0);
            }
        }
    }
}
