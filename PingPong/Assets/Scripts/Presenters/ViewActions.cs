﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ViewActions
{
    public enum ViewAction
    {
        Undefined,
        MovePlayer,
        OnBallMovedOut,
        OnBallCollidedWithPlayer,
        OnBallCollidedWithWall
    }
}