﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ViewActions;

namespace PresentersInterfaces
{
    public interface IView
    {
        void Show();
        void Hide();
        void Init();

        void Bind(Action<ViewAction, object> actionCallback);
    }
}
