﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameRulesInterfaces
{
    public interface IPlayer
    {
        int GetId();
        float GetRocketSize();
        float GetSpeed();
        int GetMoveDirection();
        void SetMoveDirection(int direction);
    }
}
