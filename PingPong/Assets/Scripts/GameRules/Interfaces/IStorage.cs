﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameRulesInterfaces
{
    public interface ISorage
    {
        int GetInt(string name);
        void SetInt(string name, int data);
    }
}
