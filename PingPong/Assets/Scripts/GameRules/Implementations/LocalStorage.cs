﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameRulesInterfaces;
using System;
using GameRulesActions;

namespace GameRulesImpl
{
    public class LocalStorage : ISorage
    {
        public int GetInt(string name)
        {
            return PlayerPrefs.GetInt(name, 0);
        }

        public void SetInt(string name, int data)
        {
            PlayerPrefs.SetInt(name, data);
        }
    }
}
