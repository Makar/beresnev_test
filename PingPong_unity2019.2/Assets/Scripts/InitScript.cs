﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameRulesInterfaces;
using GameRulesImpl;
using PresentersInterfaces;
using PresentersImpl;

public class InitScript : MonoBehaviour
{
    [SerializeField] Camera _mainCamera;

    [SerializeField] ViewBase _controlsView;
    [SerializeField] ViewBase _mobileControlsView;

    [SerializeField] ViewBase _playersView;
    [SerializeField] ViewBase _ballsView;
    [SerializeField] ViewBase _mapView;
    [SerializeField] ViewBase _scoreView;

    private IPresenter _controlsPresenter;
    private IPresenter _playersPresenter;
    private IPresenter _ballsPresenter;
    private IPresenter _mapPresenter;
    private IPresenter _scorePresenter;

    private IGameRules _gameRules;

    private const int PLAYERS_COUNT = 2;
    private const int BALLS_COUNT = 1;

    public void Start()
    {
        _gameRules = new GameRulesBase(new LocalStorage());

        if (Application.platform == RuntimePlatform.Android)
        {
            _controlsPresenter = new ControlsPresenter(_mobileControlsView, _gameRules);
        }
        else
        {
            _controlsPresenter = new ControlsPresenter(_controlsView, _gameRules);
        }

            _playersPresenter = new PlayersPresenter(_playersView, _gameRules);
        _ballsPresenter = new BallsPresenter(_ballsView, _gameRules);
        _mapPresenter = new MapPresenter(_mapView, _gameRules);
        _scorePresenter = new ScorePresenter(_scoreView, _gameRules);

        _controlsPresenter.Init();
        _playersPresenter.Init();
        _ballsPresenter.Init();
        _mapPresenter.Init();
        _scorePresenter.Init();

        float verticalExtent = _mainCamera.orthographicSize;
        float horizontalExtent = verticalExtent * Screen.width / Screen.height;

        _gameRules.Init(horizontalExtent, verticalExtent, BALLS_COUNT, PLAYERS_COUNT);
    }
}
