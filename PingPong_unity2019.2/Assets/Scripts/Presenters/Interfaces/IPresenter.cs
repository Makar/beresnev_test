﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ViewActions;

namespace PresentersInterfaces
{
    public interface IPresenter
    {
        void Show();
        void Hide();
        void Init();
    }
}
