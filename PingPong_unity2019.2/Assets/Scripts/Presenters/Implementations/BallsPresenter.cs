﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ViewActions;
using GameRulesInterfaces;
using PresentersInterfaces;
using GameRulesActions;

namespace PresentersImpl
{
    public class BallsPresenter : PresenterBase
    {
        public struct BallPlayerCollision
        {
            public int BallId;
            public int PlayerId;

            public BallPlayerCollision(int ballId, int playerId)
            {
                BallId = ballId;
                PlayerId = playerId;
            }
        }

        public BallsPresenter(IView view, IGameRules gameRules) : base(view, gameRules)
        {
        }

        protected override void GameRulesActionCallback(GameRulesAction action, object data)
        {
            if (action == GameRulesAction.UpdateBall)
            {
                if (data is IBall ball)
                {
                    UpdateBall(ball);
                }
            }

            if (action == GameRulesAction.ResetBall)
            {
                if (data is IBall ball)
                {
                    ResetBall(ball);
                }
            }

            if (action == GameRulesAction.UpdateMap)
            {
                if (data is IMap map)
                {
                    UpdateBorders(map);
                }
            }

            if (action == GameRulesAction.RemoveBall)
            {
                if (data is IBall ball)
                {
                    RemoveBall(ball);
                }
            }
        }

        protected override void ViewActionCallback(ViewAction action, object data)
        {
            if (action == ViewAction.OnBallMovedOut)
            {
                _gameRules.OnBallMovedOut((int)data);
            }

            if (action == ViewAction.OnBallCollidedWithPlayer)
            {
                if (data is BallPlayerCollision collision)
                {
                    _gameRules.OnBallCollidedWithPlayer(collision.BallId, collision.PlayerId);
                }
            }

            if (action == ViewAction.OnBallCollidedWithWall)
            {
                _gameRules.OnBallCollidedWithWall((int)data);
            }
        }

        private void UpdateBall(IBall ball)
        {
            (_view as BallsView).UpdateBall(ball.GetId(), ball.GetSpeed(), ball.GetMoveDirection());
        }

        private void ResetBall(IBall ball)
        {
            (_view as BallsView).ResetBall(ball.GetId(), ball.GetSpeed(), ball.GetMoveDirection());
        }

        private void UpdateBorders(IMap map)
        {
            (_view as BallsView).UpdateBorders(map.GetTop(), map.GetBottom(), map.GetLeft(), map.GetRight());
        }

        private void RemoveBall(IBall ball)
        {
            (_view as BallsView).RemoveBall(ball.GetId());
        }
    }
}
