﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace PresentersImpl
{
    public class ScoreView : ViewBase
    {
        [SerializeField] private Text _currentScoreLabel;
        [SerializeField] private Text _bestScoreLabel;

        public void UpdateCurrentScore(int score)
        {
            _currentScoreLabel.text = score.ToString();
        }

        public void UpdateBestScore(int score)
        {
            _bestScoreLabel.text = "Best score: " + score.ToString();
        }
    }
}
