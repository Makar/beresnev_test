﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PresentersInterfaces;
using GameRulesInterfaces;
using ViewActions;
using GameRulesActions;

namespace PresentersImpl
{
    public abstract class PresenterBase : IPresenter
    {
        protected IView _view;
        protected IGameRules _gameRules;

        public PresenterBase(IView view, IGameRules gameRules)
        {
            _gameRules = gameRules;
            _gameRules.AddListener(GameRulesActionCallback);

            _view = view;
            _view.Bind(ViewActionCallback);
        }

        protected abstract void ViewActionCallback(ViewAction action, object data);
        protected abstract void GameRulesActionCallback(GameRulesAction action, object data);

        public void Hide()
        {
            _view.Hide();
        }

        public void Init()
        {
            _view.Init();
        }

        public void Show()
        {
            _view.Show();
        }
    }
}
