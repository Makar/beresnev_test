﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ViewActions;
using GameRulesInterfaces;
using PresentersInterfaces;
using GameRulesActions;

namespace PresentersImpl
{
    public class ControlsPresenter : PresenterBase
    {
        public ControlsPresenter(IView view, IGameRules gameRules) : base(view, gameRules)
        {
        }

        protected override void GameRulesActionCallback(GameRulesAction action, object data)
        {
            
        }

        protected override void ViewActionCallback(ViewAction action, object data)
        {
            if (action == ViewAction.MovePlayer)
            {
                _gameRules.MovePlayer((int)data);
            }
        }
    }
}
