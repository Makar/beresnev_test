﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace PresentersImpl
{
    public class BallView : ViewBase
    {
        [SerializeField] private float _ballRadius;

        private int _id;
        private float _speed;
        private Vector2 _direction;

        private float _topBorder;
        private float _bottomBorder;
        private float _leftBorder;
        private float _rightBorder;

        public Action<int> OnBallMovedOut;
        public Action<int, int> OnBallCollidedWithPlayer;
        public Action<int> OnBallCollidedWithWall;

        public void Init(int id, float speed, Vector2 direction)
        {
            _id = id;
            _speed = speed;
            _direction = direction;
        }

        public void UpdateInfo(float speed, Vector2 direction)
        {
            _speed = speed;
            _direction = direction;
        }

        public int GetId()
        {
            return _id;
        }

        public void UpdateBorders(float top, float bottom, float left, float right)
        {
            _topBorder = top;
            _bottomBorder = bottom;
            _leftBorder = left;
            _rightBorder = right;
        }

        public void Update()
        {
            Vector3 newPosition = transform.position;


            newPosition.x += Time.deltaTime * _speed * _direction.normalized.x;
            newPosition.y += Time.deltaTime * _speed * _direction.normalized.y;

            if ((newPosition.x - _ballRadius <= _leftBorder && _direction.x < 0) || (newPosition.x + _ballRadius >= _rightBorder && _direction.x > 0))
            {
                OnBallCollidedWithWall?.Invoke(_id);
            }

            if (newPosition.y > _topBorder)
            {
                OnBallMovedOut?.Invoke(_id);
                return;
            }

            if (newPosition.y < _bottomBorder)
            {
                OnBallMovedOut?.Invoke(_id);
                return;
            }

            transform.position = newPosition;
        }

        private void OnTriggerEnter2D(Collider2D collision)
        {
            if (collision.gameObject.layer == LayerMask.NameToLayer("Players"))
            {
                PlayerView player = collision.gameObject.GetComponent<PlayerView>();

                if (player != null)
                {
                    OnBallCollidedWithPlayer?.Invoke(_id, player.GetId());
                }
            }
        }
    }
}
