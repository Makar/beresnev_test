﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PresentersImpl
{
    public class PlayerView : ViewBase
    {
        [SerializeField] private float _playerWidth;

        private int _id;
        private float _speed;
        private int _direction;

        private float leftBorder;
        private float rightBorder;

        public void Init(int id, float speed, int direction)
        {
            _id = id;
            _speed = speed;
            _direction = direction;
        }

        public void UpdateInfo(float speed, int direction)
        {
            _speed = speed;
            _direction = direction;
        }

        public void UpdateBorders(float left, float right)
        {
            leftBorder = left;
            rightBorder = right;
        }

        public int GetId()
        {
            return _id;
        }

        public int GetDirection()
        {
            return _direction;
        }

        public void Update()
        {
            if (_direction != 0)
            {
                Vector3 newPosition = transform.position;
                newPosition.x += Time.deltaTime * _speed * _direction;

                newPosition.x = newPosition.x < leftBorder + _playerWidth / 2 ? leftBorder + _playerWidth / 2 : newPosition.x;
                newPosition.x = newPosition.x > rightBorder - _playerWidth / 2 ? rightBorder - _playerWidth / 2 : newPosition.x;

                transform.position = newPosition;
            }
        }
    }
}
