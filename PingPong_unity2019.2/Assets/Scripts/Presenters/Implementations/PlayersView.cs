﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PresentersImpl
{
    public class PlayersView : ViewBase
    {
        [SerializeField] private PlayerView _plyerViewPrefab;
        [SerializeField] private Transform _container;

        private List<PlayerView> _players;

        private float topBorder;
        private float bottomBorder;
        private float leftBorder;
        private float rightBorder;

        public override void Init()
        {
            _players = new List<PlayerView>();
        }

        public void UpdatePlayer(int id, float speed, int direction)
        {
            foreach (PlayerView player in _players)
            {
                if (player.GetId() == id)
                {
                    player.UpdateInfo(speed, direction);
                    return;
                }
            }

            CreateNewPlayer(id, speed, direction);
        }

        public void UpdateBorders(float top, float bottom, float left, float right)
        {
            topBorder = top;
            bottomBorder = bottom;
            leftBorder = left;
            rightBorder = right;

            foreach (PlayerView player in _players)
            {
                player.UpdateBorders(left, right);
            }
        }

        private void CreateNewPlayer(int id, float speed, int direction)
        {
            PlayerView player = GameObject.Instantiate<PlayerView>(_plyerViewPrefab, _container);
            player.Init(id, speed, direction);
            player.UpdateBorders(leftBorder, rightBorder);

            if (id == 0)
            {
                player.transform.position = new Vector2(0f, bottomBorder);
            }
            else
            {
                player.transform.position = new Vector2(0f, topBorder);
            }

            _players.Add(player);
        }

    }
}
