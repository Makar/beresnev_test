﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ViewActions;

namespace PresentersImpl
{
    public class BallsView : ViewBase
    {
        [SerializeField] private BallView _ballViewPrefab;
        [SerializeField] private Transform _container;

        private List<BallView> _balls;

        private float topBorder;
        private float bottomBorder;
        private float leftBorder;
        private float rightBorder;

        public override void Init()
        {
            _balls = new List<BallView>();
        }

        public void UpdateBall(int id, float speed, Vector2 direction)
        {
            foreach (BallView ball in _balls)
            {
                if (ball.GetId() == id)
                {
                    ball.UpdateInfo(speed, direction);
                    return;
                }
            }

            CreateNewPlayer(id, speed, direction);
        }

        public void RemoveBall(int id)
        {
            foreach (BallView ball in _balls)
            {
                if (ball.GetId() == id)
                {
                    _balls.Remove(ball);
                    Destroy(ball.gameObject);
                    return;
                }
            }
        }

        public void ResetBall(int id, float speed, Vector2 direction)
        {
            foreach (BallView ball in _balls)
            {
                if (ball.GetId() == id)
                {
                    ball.UpdateInfo(speed, direction);
                    ball.transform.position = new Vector2(0f, 0f);
                    return;
                }
            }

            CreateNewPlayer(id, speed, direction);
        }

        public void UpdateBorders(float top, float bottom, float left, float right)
        {
            topBorder = top;
            bottomBorder = bottom;
            leftBorder = left;
            rightBorder = right;

            foreach (BallView player in _balls)
            {
                player.UpdateBorders(top, bottom, left, right);
            }
        }

        private void CreateNewPlayer(int id, float speed, Vector2 direction)
        {
            BallView ball = GameObject.Instantiate<BallView>(_ballViewPrefab, _container);
            ball.Init(id, speed, direction);
            ball.UpdateBorders(topBorder, bottomBorder, leftBorder, rightBorder);
            ball.OnBallMovedOut += OnBallMovedOut;
            ball.OnBallCollidedWithPlayer += OnBallColidedWithPlayer;
            ball.OnBallCollidedWithWall += OnBallColidedWithWall;
            ball.transform.position = new Vector2(0f, 0f);

            _balls.Add(ball);
        }

        private void OnBallMovedOut(int id)
        {
            _action(ViewAction.OnBallMovedOut, id);
        }

        private void OnBallColidedWithPlayer(int ballId, int playerId)
        {

            _action(ViewAction.OnBallCollidedWithPlayer, new BallsPresenter.BallPlayerCollision(ballId, playerId));
        }

        private void OnBallColidedWithWall(int id)
        {
            _action(ViewAction.OnBallCollidedWithWall, id);
        }
    }
}
