﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameRulesInterfaces;
using System;
using GameRulesActions;

namespace GameRulesImpl
{
    public class BallBase : IBall
    {
        private int _id;
        private float _speed;
        private Vector2 _moveDirection;

        //минимальное направление по Y, чтобы мячик летал больше по вертикали 
        private float _minDirectionY = 0.5f;
        private float _maxDirectionX = 0.5f;

        public BallBase(int id, float speed, Vector2 moveDirection)
        {
            _id = id;
            _speed = speed;
            SetMoveDirection(moveDirection);
        }

        public Vector2 GetMoveDirection()
        {
            return _moveDirection;
        }

        public int GetId()
        {
            return _id;
        }

        public float GetSpeed()
        {
            return _speed;
        }

        public void SetMoveDirection(Vector2 direction)
        {
            if (Mathf.Abs(direction.y) < _minDirectionY)
            {
                direction.y = _minDirectionY * (direction.y > 0 ? 1 : -1);
            }

            if (Mathf.Abs(direction.x) > _maxDirectionX)
            {
                direction.x = _maxDirectionX * (direction.x > 0 ? 1 : -1);
            }

            _moveDirection = direction;
        }

        public void SetSpeed(float speed)
        {
            _speed = speed;
        }
    }
}
