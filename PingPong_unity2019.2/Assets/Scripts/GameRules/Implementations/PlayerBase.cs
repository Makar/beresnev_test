﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameRulesInterfaces;
using System;
using GameRulesActions;

namespace GameRulesImpl
{
    public class PlayerBase : IPlayer
    {
        private int _id;
        private float _speed;
        private int _moveDirection;

        public PlayerBase(int id, float speed)
        {
            _id = id;
            _speed = speed;
        }

        public int GetId()
        {
            return _id;
        }

        public int GetMoveDirection()
        {
            return _moveDirection;
        }

        public float GetRocketSize()
        {
            throw new NotImplementedException();
        }

        public float GetSpeed()
        {
            return _speed;
        }

        public void SetMoveDirection(int direction)
        {
            _moveDirection = direction;
        }
    }
}
