﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameRulesInterfaces;
using System;
using GameRulesActions;

namespace GameRulesImpl
{
    public class MapBase : IMap
    {
        private float _top;
        private float _bottom;
        private float _left;
        private float _right;

        public MapBase(float top, float bottom, float left, float right)
        {
            _top = top;
            _bottom = bottom;
            _left = left;
            _right = right;
        }

        public float GetBottom()
        {
            return _bottom;
        }

        public float GetLeft()
        {
            return _left;
        }

        public float GetRight()
        {
            return _right;
        }

        public float GetTop()
        {
            return _top;
        }
    }
}
