﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameRulesInterfaces;
using System;
using GameRulesActions;

namespace GameRulesImpl
{
    //Класс основной логики игры, можно переопределять методы для реализации различный вариаций игры
    public class GameRulesBase : IGameRules
    {
        private Action<GameRulesAction, object> _doAction;
        private List<IPlayer> _players;
        private List<IBall> _balls;
        private IMap _map;
        private ISorage _storage;

        private int _currentScore;
        private int _ballsStartCount;
        private int _playersStartCount;

        private const float PLAYER_SPEED = 20f;
        private const float BALL_MIN_SPEED = 8f;
        private const float BALL_MAX_SPEED = 12f;
        private const float BALL_ACCELERATION = 1.01f;

        public GameRulesBase(ISorage storage)
        {
            _storage = storage;
        }

        public void AddListener(Action<GameRulesAction, object> action)
        {
            _doAction += action;
        }

        public void Init(float screenWidth, float screenHeight, int ballsCount, int playersCount)
        {
            _players = new List<IPlayer>();
            _balls = new List<IBall>();
            _map = new MapBase(screenHeight - 1.0f, -screenHeight + 1.0f, -screenWidth + 0.5f, screenWidth - 0.5f);
            _currentScore = 0;
            _playersStartCount = playersCount;
            _ballsStartCount = ballsCount;

            _doAction?.Invoke(GameRulesAction.UpdateMap, _map);

            InitPlayers();
            InitBalls();

            UpdatePlayers();
            UpdateBalls();

            _doAction?.Invoke(GameRulesAction.UpdateBestScore, _storage.GetInt("best_score"));
            _doAction?.Invoke(GameRulesAction.UpdateCurrentScore, _currentScore);
        }

        public void MovePlayer(int direction)
        {
            foreach (IPlayer player in _players)
            {
                player.SetMoveDirection(direction);
            }

            UpdatePlayers();
        }

        public void OnBallCollidedWithPlayer(int ballId, int playerId)
        {
            IBall ball = GetBallById(ballId);
            IPlayer player = GetPlayerById(playerId);

            if (ball != null)
            {
                Vector2 direction = ball.GetMoveDirection();

                if (player != null && player.GetMoveDirection() != 0)
                {
                    //делаем более реалистичный отскок мяча от ракетки
                    direction += new Vector2(player.GetMoveDirection(), 0f);
                }

                //при отскоке меняем направление полета мяча
                direction.y *= -1;
                ball.SetMoveDirection(direction);

                //С каждым удором увеличиваем скорость мяча чтобы было интереснее играть
                ball.SetSpeed(ball.GetSpeed() * BALL_ACCELERATION);

                _doAction?.Invoke(GameRulesAction.UpdateBall, ball);

                _currentScore++;
                _doAction?.Invoke(GameRulesAction.UpdateCurrentScore, _currentScore);
            }
        }

        public void OnBallCollidedWithWall(int ballId)
        {
            IBall ball = GetBallById(ballId);

            if (ball != null)
            {
                Vector2 direction = ball.GetMoveDirection();

                //при отскоке меняем направление полета мяча
                direction.x *= -1;
                ball.SetMoveDirection(direction);
                _doAction?.Invoke(GameRulesAction.UpdateBall, ball);
            }
        }

        public void OnBallMovedOut(int ballId)
        {
            foreach(IBall ball in _balls)
            {
                if (ball.GetId() == ballId)
                {
                    _balls.Remove(ball);
                    _doAction?.Invoke(GameRulesAction.RemoveBall, ball);
                    break;
                }
            }

            if (_balls.Count == 0)
            {
                Restart();
            }
        }

        private void Restart()
        {
            InitBalls();

            UpdateBalls();

            SaveScore();
        }

        private void InitPlayers()
        {
            for(int i = 0; i < _playersStartCount; i++)
            {
                _players.Add(new PlayerBase(i, PLAYER_SPEED));
            }
        }

        private void UpdatePlayers()
        {
            foreach (IPlayer player in _players)
            {
                _doAction?.Invoke(GameRulesAction.UpdatePlayer, player);
            }
        }

        private void InitBalls()
        {
            for (int i = 0; i < _ballsStartCount; i++)
            {
                _balls.Add(new BallBase(i, UnityEngine.Random.Range(BALL_MIN_SPEED, BALL_MAX_SPEED), new Vector2(UnityEngine.Random.Range(-1f, 1f), UnityEngine.Random.Range(-1f, 1f))));
            }
        }

        private void UpdateBalls()
        {
            foreach (IBall ball in _balls)
            {
                _doAction?.Invoke(GameRulesAction.ResetBall, ball);
            }
        }

        private IPlayer GetPlayerById(int id)
        {
            foreach (IPlayer player in _players)
            {
                if (player.GetId() == id)
                {
                    return player;
                }
            }

            return null;
        }

        private IBall GetBallById(int id)
        {
            foreach (IBall ball in _balls)
            {
                if (ball.GetId() == id)
                {
                    return ball;
                }
            }

            return null;
        }

        private void SaveScore()
        {
            if (_currentScore > _storage.GetInt("best_score"))
            {
                _storage.SetInt("best_score", _currentScore);
                _doAction?.Invoke(GameRulesAction.UpdateBestScore, _currentScore);
            }
            _currentScore = 0;
            _doAction?.Invoke(GameRulesAction.UpdateCurrentScore, _currentScore);
        }
    }
}
