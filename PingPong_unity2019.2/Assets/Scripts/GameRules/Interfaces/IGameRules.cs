﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameRulesActions;

namespace GameRulesInterfaces
{
    public interface IGameRules
    {
        void Init(float scrrenWidth, float screenHeaight, int ballsCount, int playersCount);
        void AddListener(Action<GameRulesAction, object> action);
        void MovePlayer(int direction);
        void OnBallMovedOut(int ballId);
        void OnBallCollidedWithPlayer(int ballId, int playerId);
        void OnBallCollidedWithWall(int ballId);
    }
}
