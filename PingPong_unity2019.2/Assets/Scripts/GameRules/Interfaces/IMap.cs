﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameRulesInterfaces
{
    public interface IMap
    {
        float GetTop();
        float GetBottom();
        float GetLeft();
        float GetRight();
    }
}