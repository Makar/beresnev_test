﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameRulesInterfaces
{
    public interface IBall
    {
        int GetId();
        float GetSpeed();
        void SetSpeed(float speed);
        Vector2 GetMoveDirection();
        void SetMoveDirection(Vector2 direction);

    }
}
