﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameRulesActions
{
    public enum GameRulesAction
    {
        Undefined,
        UpdateBall,
        RemoveBall,
        ResetBall,
        UpdatePlayer,
        UpdateMap,
        UpdateCurrentScore,
        UpdateBestScore
    }
}
